<!DOCTYPE html>
<!--// OPEN HTML //-->
<html <?php language_attributes(); ?> class="no-js">
<head>
<?php
	$options = get_option('imic_options');
   	/** Theme layout design * */
  	$bodyClass = ($options['site_layout'] == 'boxed') ? ' boxed' : '';
   	$style='';
    if($options['site_layout'] == 'boxed'){
    	if (!empty($options['upload-repeatable-bg-image']['id'])) {
       		$style = ' style="background-image:url(' . $options['upload-repeatable-bg-image']['url'] . '); background-repeat:repeat; background-size:auto;"';
     	} else if (!empty($options['full-screen-bg-image']['id'])) {
            $style = ' style="background-image:url(' . $options['full-screen-bg-image']['url'] . '); background-repeat: no-repeat; background-size:cover;"';
        }
       	else if(!empty($options['repeatable-bg-image'])) {
            $style = ' style="background-image:url(' . get_template_directory_uri() . '/images/patterns/' . $options['repeatable-bg-image'] . '); background-repeat:repeat; background-size:auto;"';
        }
  	}
?>
<!--// SITE TITLE //-->
<title>
<?php
	if ( defined('WPSEO_VERSION') ) {
   		wp_title('');
 	} else {
    	bloginfo('name'); ?> <?php wp_title(' - ', true, 'left');
    }
?>
</title>
<!--// SITE META //-->
<meta charset="<?php bloginfo('charset'); ?>" />
<!-- Mobile Specific Metas
================================================== -->
<?php if (isset($options['switch-responsive'])&&$options['switch-responsive'] == 1){
	$switch_zoom_pinch = (isset($options['switch-zoom-pinch']))?$options['switch-zoom-pinch']:''; ?>
	<?php if ($switch_zoom_pinch == 1){ ?>
    	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0">
   	<?php } else { ?>
    	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
   	<?php } ?>
   	<meta name="format-detection" content="telephone=no">
<?php } ?>
<!--// PINGBACK & FAVICON //-->
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<?php if (function_exists( 'wp_site_icon') && has_site_icon()) {
	echo '<link rel="shortcut icon" href="'.get_site_icon_url().'" />';
}
else
{
	if (isset($options['custom_favicon']) && $options['custom_favicon'] != "") { ?>
    	<link rel="shortcut icon" href="<?php echo esc_url($options['custom_favicon']['url']); ?>" />
<?php }
}
if (isset($options['iphone_icon']) && $options['iphone_icon'] != "")
{ ?>
 	<link rel="apple-touch-icon-precomposed" href="<?php echo $options['iphone_icon']['url']; ?>"><?php
}
if (isset($options['iphone_icon_retina']) && $options['iphone_icon_retina'] != "")
{ ?>
  	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $options['iphone_icon_retina']['url']; ?>"><?php
}
if (isset($options['ipad_icon']) && $options['ipad_icon'] != "")
{ ?>
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $options['ipad_icon']['url']; ?>"><?php
}
if (isset($options['ipad_icon_retina']) && $options['ipad_icon_retina'] != "")
{ ?>
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $options['ipad_icon_retina']['url']; ?>"><?php
}
$offset = get_option('timezone_string');
if($offset=='') {
	$offset = "Australia/Melbourne";
}
date_default_timezone_set($offset);
?>
<!-- CSS
================================================== -->
<!--[if lte IE 8]><link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/css/ie8.css" media="screen" /><![endif]-->
    <?php
	$space_before_head = (isset($options['space-before-head']))?$options['space-before-head']:'';
   	$SpaceBeforeHead = $space_before_head;
    echo $SpaceBeforeHead;
?>
<?php //  WORDPRESS HEAD HOOK 
wp_head(); ?>
</head>
<!--// CLOSE HEAD //-->
<body <?php body_class($bodyClass); echo $style;  ?>>
	<div class="body header-style<?php echo $options['header_layout']; ?>">
		<?php
        $menu_locations = get_nav_menu_locations();
        if ($options['header_layout'] == 3):
        //-- Start Top Row --
        echo '
        <div class="toprow">
   			<div class="container">
    			<div class="row">
          	 		<div class="col-md-6 col-sm-6">
            			<nav class="top-menus">
                			<ul>';
                				$socialSites = $options['header_social_links'];
                				foreach ($socialSites as $key => $value) {
									if (filter_var($value, FILTER_VALIDATE_EMAIL)) {
										echo '<li><a href="mailto:' . $value . '"><i class="fa ' . $key . '"></i></a></li>';
									}
									elseif (filter_var($value, FILTER_VALIDATE_URL)) {
										echo '<li><a href="' . $value . '" target="_blank"><i class="fa ' . $key . '"></i></a></li>';
									}
									elseif($key == 'fa-skype' && $value != '') {
										echo '<li><a href="skype:' . $value . '?call"><i class="fa ' . $key . '"></i></a></li>';
									}
                				}
                			echo'</ul>
              			</nav>
         			</div>';
                	if (!empty($menu_locations['top-menu'])) {
                    	echo'<div class="col-md-6 col-sm-6">';
            			wp_nav_menu(array('theme_location' => 'top-menu', 'menu_class' => 'top-navigation sf-menu', 'container' => '', 'walker' => new imic_mega_menu_walker));
                    	echo'
         				</div>';
                	}
                	echo'
				</div>
			</div>
		</div>';
       	//-- End Top Row --
       	endif;
		?>
     	<!-- Start Site Header -->
       	<header class="site-header">
      		<div class="topbar">
            	<div class="container hs4-cont">
                  	<div class="row">
               			<div id="top-nav-clone"></div>
                  			<div class="col-md-4 col-sm-6 col-xs-8  gunaimag">
                           		<h1 class="logo">
                              		<?php
                                    global $imic_options;
									if(isset($imic_options['logo_alt_text']) && $imic_options['logo_alt_text'] != "") { ?>
                                    <?php $logoalt = esc_html($imic_options['logo_alt_text']); } else { $logoalt = 'Logo'; } ?>
                                    <?php
                                    if (!empty($imic_options['logo_upload']['url'])) { ?>
                                       <a href="<?php echo esc_url( home_url() ); ?>" class="default-logo" title="<?php echo $logoalt; ?>"><img src="<?php echo $imic_options['logo_upload']['url']; ?>" alt="<?php echo $logoalt; ?>"></a>
                                    <?php } else { ?>
                                        <a href="<?php echo esc_url( home_url() ); ?>" title="<?php echo $logoalt; ?>" class="default-logo theme-blogname"><?php echo bloginfo('name'); ?></a>
                                    <?php }
                                    ?>
                                    <?php
                                    global $imic_options;
                                    if (!empty($imic_options['retina_logo_upload']['url'])) { ?>
                                        <a href="<?php echo esc_url( home_url() ); ?>" title="<?php echo $logoalt; ?>" class="retina-logo"><img src="<?php echo $imic_options['retina_logo_upload']['url']; ?>" alt="<?php echo $logoalt; ?>" width="<?php echo $imic_options['retina_logo_width']; ?>" height="<?php echo $imic_options['retina_logo_height']; ?>"></a>
                                    <?php } elseif (!empty($imic_options['logo_upload']['url'])) { ?>
                                        <a href="<?php echo esc_url( home_url() ); ?>" title="<?php echo $logoalt; ?>" class="retina-logo"><img src="<?php echo $imic_options['logo_upload']['url']; ?>" alt="<?php echo $logoalt; ?>"></a>
                                   <?php } else { ?>
                                        <a href="<?php echo esc_url( home_url() ); ?>" title="<?php echo $logoalt; ?>" class="retina-logo theme-blogname"><?php echo bloginfo('name'); ?></a>
                                    <?php }
                                    ?>
                                </h1>
                            </div>
                            <?php
                            if (!empty($options['header_layout'])):
                                echo '<div class="col-md-8 col-sm-6 col-xs-4 hs4-menu">';
								if ($options['enable-top-menu'] == 1){echo '<div class="enabled-top-mobile">';}
                                if ($options['header_layout'] != 3):
                                    if (!empty($menu_locations['top-menu'])):
                                        wp_nav_menu(array('theme_location' => 'top-menu', 'menu_class' => 'top-navigation sf-menu', 'container' => 'div','container_class' => 'tinymenu', 'walker' => new imic_mega_menu_walker));
										
                                    endif;
									else:
									if(isset($options['header3_textarea'])&&$options['header3_textarea']!='')
									{
										echo '<div class="top-search hidden-sm hidden-xs">'.do_shortcode($options['header3_textarea']).'</div>';
									}
									else
									{
                                    echo '<div class="top-search hidden-sm hidden-xs">
            	           			<form method="get" id="searchform" action="' . home_url() . '">
                	    			<div class="input-group">
                 					<span class="input-group-addon"><i class="fa fa-search"></i></span>
                					<input type="text" class="form-control" name="s" id="s" placeholder="' . __('Type your keywords...', 'framework') . '">
                 	   				</div>
              	          			</form>
                          			</div>';
									}
                                	endif;
                                	echo '<a href="#" class="visible-sm visible-xs menu-toggle"><i class="fa fa-bars"></i> ' . $options['mobile_menu_text'] .'</a>';
									if ($options['enable-top-menu'] == 1){echo '</div>';}
                            	echo '</div>';
                            	endif;
                            	?>
                        </div>
                    </div>
                </div>
				<?php if ($options['header_layout'] != 4) { ?>
                    <?php if (!empty($menu_locations['primary-menu'])) { ?>
                        <div class="main-menu-wrapper">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <nav class="navigation">
                  <?php wp_nav_menu(array('theme_location' => 'primary-menu', 'menu_class' => 'sf-menu', 'container' => '', 'walker' => new imic_mega_menu_walker)); ?>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                    <?php } ?>
                <?php } ?>
            </header>
            <!-- End Site Header -->
            <?php
            $term = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy'));
           	$flag=imic_cat_count_flag();
            $page_for_posts = get_option('page_for_posts');
			$show_on_front = get_option('show_on_front');
			if (is_home()) {
                $id = $page_for_posts;
            } elseif (is_404() || is_search()) {
                $id = '';
            }
     		elseif(function_exists( 'is_shop' )&& is_shop()) {
             	$id= get_option('woocommerce_shop_page_id');
           	} 
           	elseif ($flag==0) {
            	$id=''; 
           	}
 			else {
           		$id = get_the_ID();
            }
            if ((!is_front_page()) || $show_on_front == 'posts'||(!is_page_template('template-home.php')&&!is_page_template('template-h-second.php')&&!is_page_template('template-h-third.php')&&!is_page_template('template-home-pb.php'))) {
                if (is_404() || is_search()||$flag==0) {
                    $custom = array();
                } else {
                    $custom = get_post_custom($id);
                }
				$header_image = get_post_meta($id,'imic_header_image',true);
				$header_title = get_post_meta($id,'imic_post_page_custom_title',true);

				if (is_category() || !empty($term->term_id)) {
               		global $cat;
                    if(!empty($cat)){
                    	$term_taxonomy='category';
                       	$t_id = $cat; // Get the ID of the term we're editing
                  	}else{
                      	$term_taxonomy=get_query_var('taxonomy');
                      	$t_id = $term->term_id; // Get the ID of the term we're editing
              		}
                   	$header_image  = get_option($term_taxonomy . $t_id . "_image_term_id"); // Do the check
           		}
                $default_header_image = $imic_options['header_image']['url'];
                if (!empty($header_image)) {
                   if (is_category() || !empty($term->term_id)) {
                        $src[0] = $header_image;
                    }
                  	else {
                    	$src = wp_get_attachment_image_src($header_image, 'Full');
                	}
                } else {
		  			$src[0] = $default_header_image;
				}?>
                <!-- Start Nav Backed Header -->
                <?php $header_options = get_post_meta($id,'imic_pages_Choose_slider_display',true); 
						$height = get_post_meta($id,'imic_pages_slider_height',true);
						$height = ($height == '') ? '150' : $height;
						$breadpad = $height - 60;
						if($header_options==0||$header_options=='') { ?>
                        <?php 
						echo '<style type="text/css">' . "\n";
						echo '.body ol.breadcrumb{padding-top:'.$breadpad.'px;}';
						echo "</style>" . "\n"; ?>

                    <div class="container-fluid top_wrapper">
                        <div class="nav-backed-header animated_head animated slideInDown delay-5s" style="background-image:url(<?php echo $src[0]; ?>);">
                            <div class="overlay" style="background:rgba(79, 79, 79, 0.75"></div>
                            <div class="container">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 highlights_inner">
                                    <p class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><?php echo $header_title; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } elseif($header_options==3||$header_options=='') { ?>
                        <?php 
						$color = get_post_meta($id,'imic_pages_banner_color',true);
						$color = ($color!='')?$color:'';
						echo '<style type="text/css">' . "\n";
						echo '.body ol.breadcrumb{padding-top:'.$breadpad.'px;}';
						echo "</style>" . "\n"; ?>
                <div class="nav-backed-header parallax" style="background-color:<?php echo $color; ?>;">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <ol class="breadcrumb">
                                    <?php
                                    if (function_exists('bcn_display_list')) {
                                        bcn_display_list();
                                    }
                                    ?>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                    <?php } else {
					
			include(locate_template('pages_slider.php')); } ?>
                <!-- End Nav Backed Header -->
                    <?php
                    /**   Start Content* */
                    echo'<div class="main" role="main">
                     <div id="content" class="content full">';
                } ?>




