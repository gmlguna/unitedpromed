<?php
/*
  Template Name: Example
*/
get_header();
?>
<nav>
    <ul>
        <li class="depth-1">
            <a class="slide header" href="#">Identity</a>
            <ul class="slideContent" >
                <li class="depth-2"><a href="#">Link</a></li>
            </ul>
        </li>
        <li class="depth-1" >
            <a class="slide header" href="#">Products</a>
            <ul class="slideContent">
                <li class="depth-2">
                    <a href="#">Widgets</a>
                </li>
                <li class="depth-2">
                    <a class="slide header" href="#">Desktop Apps</a>
                    <ul class="slideContent ">
                        <li class="depth-3">
                            <a href="#">Email App</a>
                        </li>
                        <li class="depth-3">
                            <a  class="slide header" href="#">Editing App</a>
                            <ul class="slideContent">
                                <li class="depth-4">
                                    <a href="#">Color</a>
                                </li>
                                <li class="depth-4">
                                    <a href="#">Icons</a>
                                </li>
                                <li class="depth-4">
                                    <a href="#">Typeface</a>
                                </li>
                                <li class="depth-4">
                                    <a href="#">Prototype</a>
                                </li>
                                <li class="depth-4">
                                    <a class="slide header" href="#">Development</a>
                                    <ul class="slideContent">
                                        <li class="depth-5" ><a  href="#">Link</a></li>
                                        <li class="depth-5" ><a  href="#">Link</a></li>
                                        <li class="depth-5" ><a  href="#">Link</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="depth-2">
                    <a href="#">Mobile Apps</a>
                </li>
            </ul>

        </li>
    </ul>

</nav>
<?php
get_footer();
?>

