<?php
/*
  Template Name: Medical
*/
get_header();
global $imic_options;
$custom_home = get_post_custom(get_the_ID());
$home_id = get_the_ID();
$pageOptions = imic_page_design('', 8); //page design options
imic_sidebar_position_module();
?>

<div class="container-fluid">
    <div class="container">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padd_top_20 our_company left">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 billing_partner">
                    <ul>
                        <li class="active">
                            <a href="http://unitedpromed.com/services/">Services Overview</a>
                        </li>
                        <li class="simple">
                            <a href="http://unitedpromed.com/medical-billing/">Medical Billing</a>
                        </li>
                        <li class="simple">
                            <a href="http://unitedpromed.com/mental-health-billing/">Mental Health Billing</a>
                        </li>
                        <ul class="nav navbar-nav">
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="{{ url('/adobe') }}">Medical Coding <i class="icon-arrow-right"></i></a>
                                    <ul class="dropdown-menu  sub-menu">
                                        <li><a href="{{ url('/expresiencedesign') }}"> Coding Services </a></li>
                                        <li><a href="{{ url('/after_effects') }}"> Work Flow Diagram </a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="{{ url('/adobe') }}">Medical Billing <i class="icon-arrow-right"></i></a>
                                    <ul class="dropdown-menu  sub-menus">
                                        <li><a href="{{ url('/expresiencedesign') }}"> Billing Services </a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="{{ url('/adobe') }}">Customized Billing <i class="icon-arrow-right"></i></a>
                                    <ul class="dropdown-menu  sub-menu">
                                        <li><a href="{{ url('/expresiencedesign') }}"> Eligibility Verifications </a></li>
                                        <li><a href="{{ url('/after_effects') }}">  Charge Entry  </a></li>
                                        <li><a href="{{ url('/expresiencedesign') }}"> Payment Posting </a></li>
                                        <li><a href="{{ url('/after_effects') }}"> AR Management  </a></li>
                                        <li><a href="{{ url('/expresiencedesign') }}"> Denial Management </a></li>
                                        <li><a href="{{ url('/after_effects') }}"> Old AR Recovery </a></li>
                                    </ul>
                                </li>
                            </ul>
                        </ul>
                    </ul>
                </div>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 padd_top_20 our_company_para">
                <div class="block_title"><h1>MEDICAL BILLING</h1></div>
                <div class="premierss_service">Premier Service</div>
                <p class="para_text"><strong>Unitedpro</strong> Premier Service is your answer to a perfect partnership. No
                    more worrying about your billing and collection problems. We specialize in handling all billing and
                    claims processing functions, so that you can spend time providing top level care to your patients.
                    Our cost-effective services will help increase your revenue while reducing operational costs.
                </p>
                <h3 style="font-size: 20px;color: #ed2f42;text-align: left;font-family:Roboto;font-weight:500;font-style:normal"
                    class="vc_custom_heading">Service Includes:</h3>
                   <span class="vc_empty_space_inner"></span>
                   <div class="wpb_content_element list only_icon" style="">
                    <ul>
                        <li class="simple para_texts"><i class="fa fa-bolt sim"></i> &nbsp; Create new patient accounts from
                            face sheets and copies of insurance cards
                        </li>
                        <li class="simple para_texts"><i class="fa fa-bolt sim"></i> &nbsp; Enter charges
                            from your superbills (mailed, delivered, faxed or  uploaded to Unitedpro)
                        </li>
                        <li class="simple para_texts"><i class="fa fa-bolt sim"></i> &nbsp;Send monthly patient statements
                        </li>
                        <li class="simple para_texts"><i class="fa fa-bolt sim"></i> &nbsp;Post payments received from
                            patient statements
                        </li>
                        <li class="simple para_texts"><i class="fa fa-bolt sim"></i> &nbsp;File electronic and hardcopy
                            insurance claims
                        </li>
                        <li class="simple para_texts"><i class="fa fa-bolt sim"></i> &nbsp;Re-file rejected claims with
                            corrections
                        </li>
                        <li class="simple para_texts"><i class="fa fa-bolt sim"></i> &nbsp;Post insurance
                            ERA/EOB/REMIT (mailed, delivered, faxed or uploaded to Unitedpro)
                        </li>
                        <li class="simple para_texts"><i class="fa fa-bolt sim"></i> &nbsp; Secondary insurance billing</li>
                        <li class="simple para_texts"><i class="fa fa-bolt sim"></i> &nbsp; Automated appointment reminders
                            via text and email
                        </li>
                        <li class="simple para_texts"><i class="fa fa-bolt sim"></i> &nbsp;Appointment scheduler</li>
                        <li class="simple para_texts"><i class="fa fa-bolt sim"></i> &nbsp;Month end reporting</li>
                        <li class="simple para_texts"><i class="fa fa-bolt sim"></i> &nbsp;Provider credentialing assistance
                        </li>
                        <li class="simple para_texts"><i class="fa fa-bolt sim"></i> &nbsp;Review past-due accounts and
                            more…
                        </li
                    </ul>
                    <hr>
                    <div class="premierss_service">Standard/Hybrid Service</div>
                    <p class="para_text"><strong>Unitedpro</strong> Unitedpro Standard Service is a unique approach to managing your medical billing. We seamlessly partner with your team to make patient scheduling and billing easy and efficient. Once you enter charges and patient payments, we handle the rest – allowing you to provide excellent patient care without all the worry.
                    </p>
                    <br>
                    <h3 style="font-size: 20px;color: #ed2f42;text-align: left;font-family:Roboto;font-weight:500;font-style:normal"
                        class="vc_custom_heading">Service Includes:</h3>
                        <span class="vc_empty_space_inner"></span>
                        <ul>
                            <li class="simple para_texts"><i class="fa fa-bolt sim"></i> &nbsp;Edit insurance claims
                            </li>
                            <li class="simple para_texts"><i class="fa fa-bolt sim"></i> &nbsp;File electronic insurance claims</li>
                            <li class="simple para_texts"><i class="fa fa-bolt sim"></i> &nbsp;File hard copy insurance claims</li>
                            <li class="simple para_texts"><i class="fa fa-bolt sim"></i> &nbsp;Post insurance payments</li>
                            <li class="simple para_texts"><i class="fa fa-bolt sim"></i> &nbsp;Post insurance remark notes
                            </li>
                            <li class="simple para_texts"><i class="fa fa-bolt sim"></i> &nbsp;Send patient statements</li>
                            <li class="simple para_texts"><i class="fa fa-bolt sim"></i> &nbsp;Follow-up on claim rejections</li>
                            <li class="simple para_texts"><i class="fa fa-bolt sim"></i> &nbsp;Review past-due accounts and more…
                            </li>
                        </ul>
                </div>
            </div>
        </div>


    </div>
</div>


<?php
while (have_posts()) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
    <?php the_content(); ?> <!-- Page Content -->
<?php
endwhile;
wp_reset_query(); //resetting the page query
?>
<?php
get_footer();
?>






